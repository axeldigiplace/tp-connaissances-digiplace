# Pré-requis du projet
Si le fichier d'initialisation `init_tp_connaissances` n'existe pas, il faut le créer
```bash
touch init_tp_connaissances
echo "git clone https://gitlab.com/axeldigiplace/tp-connaissances-digiplace.git" > init_tp_connaissances
chmod +x init_tp_connaissances
```

# Installation du projet
La commande suivante va cloner le projet
```bash
./init_tp_connaissances
```
Dans le dossier `tp-connaissances-digiplace`
```bash
sudo docker-compose up
```

# Utilisation du projet
Ouvrir avec n'importe quel IDE le dossier `tp-connaissances-digiplace/src` puis se rendre sur l'adresse IP du container **apache**

# Quelques commandes utiles
Connaître l'IP du container apache
```bash
sudo docker inspect apache
```
Généralement l'IP ressemble à: **172.19.0.3**